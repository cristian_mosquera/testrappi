/**
 * 
 */
package com.rappi.test.business.api;

import java.util.List;

import com.rappi.test.dto.TestCaseDTO;
import com.rappi.test.exceptions.TestRappiSystemException;
import com.rappi.test.exceptions.TestRappiValidationException;

/**
 * 
 * Interfaz que define el contrato para las operaciones del cubo.
 * 
 * @author <a href = "mailto:mosquerapuello@gmail.com">
 * 									Cristian José Mosquera Puello</a>
 *
 */
public interface CubeSummationService {

	/**
	 * 
	 * Método responsable de obtener los casos de pruebas.
	 *
	 * @param input
	 *            datos de entrada de los casos de prueba.
	 * @return los casos de prueba.
	 * @throws TestRappiSystemException
	 *             en caso de un error inesperado
	 * @throws TestRappiValidationException
	 *             en caso de un error de validacion de negocio.
	 */
	List<TestCaseDTO> getTestCases(String input) 
			 throws TestRappiSystemException, TestRappiValidationException;
	
	/**
	 * 
	 * Método responsable de obtener un caso de prueba.
	 *
	 * @param testCaseNumber
	 *            el numero del caso de prueba que desea obtener
	 * @param input
	 *            la entrada de datos.
	 * @return el caso de prueba.
	 * @throws TestRappiSystemException
	 *             en caso de un error inesperado.
	 * @throws TestRappiValidationException
	 *             en caso de un error de validacion de negocio
	 */
	TestCaseDTO getTestCase(int testCaseNumber, String input)
			throws TestRappiSystemException, TestRappiValidationException;
	
	
	/**
	 * 
	 * Método responsable de ejecutar un comando.
	 *
	 * @param command
	 *            el comando que desea ejecutar.
	 * @param la dimencion de la matriz.           
	 *            
	 * @return el resultado del comando ejecutado.
	 *            
	 * @throws TestRappiSystemException
	 *             en caso de un error inesperado
	 * @throws TestRappiValidationException
	 *             en caso de un error de validacion de negocio
	 */
	String executeCommand(String command, int n)
			throws TestRappiSystemException, TestRappiValidationException;
	
	
	/**
	 * 
	 * Método responsable de calcular la suma del cubo.
	 *
	 * @return el resultado del calculo de la sumarizacion.
	 * 
	 * @param input
	 *            la entrada de datos .
	 * @throws TestRappiSystemException
	 *             en caso de un error inesperado
	 * @throws TestRappiValidationException
	 *             en caso de un error de validacion de negocio
	 */
	String calculateSummation(String input)
			throws TestRappiSystemException, TestRappiValidationException;
}