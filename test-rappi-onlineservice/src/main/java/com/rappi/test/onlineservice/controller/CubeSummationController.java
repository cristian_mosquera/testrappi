/**
 * 
 */
package com.rappi.test.onlineservice.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rappi.test.business.api.CubeSummationService;
import com.rappi.test.commons.ConstantsMessages;
import com.rappi.test.exceptions.TestRappiSystemException;
import com.rappi.test.exceptions.TestRappiValidationException;
import com.rappi.test.onlineservice.response.ValidatedResponse;

/**
 * Clase encargada de manejar los eventos de interfaz de usuario.
 * 
 * @author <a href = "mailto:mosquerapuello@gmail.com">Cristian Jos� Mosquera Puello</a>
 *
 */
@Controller
@RequestMapping("/rappi" )
public class CubeSummationController {
	
	/** Recurso de mensajes. **/
	@Autowired
	protected MessageSource resource;
	
	/** Servicio de sumarizacion **/
	@Autowired
	private CubeSummationService cubeSummationService;
	
	
	/** Logger de a aplicacion. **/
	private static final Logger LOGGER = 
			LoggerFactory.getLogger(CubeSummationController.class);
	
	
	
	/**
	 * 
	 * M�todo responsable de cargar las ciudades por departamento.
	 *
	 * @param provinceId
	 *            el id del departamento.
	 * @param map
	 *            el mpaa con el modelo.
	 * @return las ciudades encontradas.
	 * @throws Exception
	 *             en caso de un error inesperado.
	 */
	@RequestMapping(value = "/calculateSummation", method = {RequestMethod.POST})
	protected @ResponseBody ValidatedResponse<String> calculateSummation
		(String input, ModelMap map, HttpServletRequest request) 
															throws Exception {
		ValidatedResponse<String> validatedResponse 
							= new ValidatedResponse<String>();
		try {
			String result = cubeSummationService.calculateSummation(input);
			validatedResponse.setContent(result);
			validatedResponse.setContainErrors(validatedResponse.containsErrors());
			return validatedResponse;
		} 
		catch (TestRappiValidationException e) {
			
			String message = resource.getMessage
					(e.getMessage(), null, Locale.getDefault());
			validatedResponse.addGeneralError(message);
			validatedResponse.setContainErrors(validatedResponse.containsErrors());
		}
		
		catch (TestRappiSystemException e) {
			String message = resource.getMessage
					(e.getPlainText(), null, Locale.getDefault());
			validatedResponse.addGeneralError(message);
			validatedResponse.setContainErrors(validatedResponse.containsErrors());
		}
		catch (Exception e) {
			String message = resource.getMessage(ConstantsMessages.
					FAILED_MESSAGE_UN_EXPECTED_ERROR, null, Locale.getDefault());
			validatedResponse.addGeneralError(message);
			validatedResponse.setContainErrors(validatedResponse.containsErrors());
			LOGGER.error(e.getMessage(), e);
		}
		
		return validatedResponse;
	}
	
	
	/**
	 * 
	 * M�todo responsable de cargar el index.
	 *
	 * @param map el mapa modelo con los datos del request.
	 * @param request el request.
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = {RequestMethod.GET})
	protected String loadIndex
		( ModelMap map, HttpServletRequest request) 
															throws Exception {
		return "index";
	}

	
	
}
