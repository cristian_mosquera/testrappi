package com.rappi.test.onlineservice.response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * 
 * Clase que envuelve una respuesta a una petición a servidor junto con mensajes de
 * validaci&oacute;n. El contenido de la respuesta está parametrizado.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 * @param <T> la respuesta del servidor
 */
public class ValidatedResponse<T> implements IValidationResultsContainer {

	/**
	 * Mapa de los mensajes de validación donde la clave es el nombre de una property a validar y el
	 * valor es el mensaje de validaci&oacute;n asociado.
	 */
	private Map<String, String> validationResult;
	
	/** Indica si la peticion genero erores. **/
	private Boolean containErrors;
	
	/** El mensaje de la cabecera del popup. **/
	private String headMessagePopup = StringUtils.EMPTY;

	/**
	 * Mapa de errores generales.
	 */
	private List<String> generalErrors;

	/**
	 * Contenido de la respuesta.
	 */
	private T content;

	/**
	 * Constructor por default.
	 */
	public ValidatedResponse() {

		this.validationResult = new HashMap<String, String>();
		this.generalErrors = new ArrayList<String>();
	}

	/**
	 * Devuelve el valor de validationResult.
	 * 
	 * @return El valor de validationResult.
	 */
	public final Map<String, String> getValidationResult() {

		return (Collections.unmodifiableMap(this.validationResult));
	}

	/**
	 * Devuelve el valor de content.
	 * 
	 * @return El valor de content.
	 */
	public final T getContent() {

		return this.content;
	}

	/**
	 * Asigna un nuevo valor a content.
	 * 
	 * @param content El valor a asignar a content.
	 */
	public final void setContent(final T content) {

		this.content = content;
	}

	/**
	 * Devuelve el valor de generalErrors.
	 * 
	 * @return El valor de generalErrors.
	 */
	public final List<String> getGeneralErrors() {

		return generalErrors;
	}

	/**
	 * Agrega un error general.
	 * 
	 * @param errorMessage el mensaje de error
	 */
	public final void addGeneralErrors(final String errorMessage) {

		this.generalErrors.add(errorMessage);
	}

	@Override
	public final void addFieldValidationMessage(final String object, final String message) {

		if (this.getValidationResult().containsKey(object)) {
			String newMessage = this.getValidationResult().get(object);

			newMessage += " " + message;

			this.validationResult.put(object, newMessage);
		} else {
			this.validationResult.put(object, message);
		}
	}

	@Override
    public final void addGeneralValidationMessage(final String message) {

		this.generalErrors.add(message);
	}
	
	@Override
	public final boolean containsErrors() {
		
		return (!this.generalErrors.isEmpty() || !this.getValidationResult().isEmpty());
	}

	
    /**
     * Asigna un nuevo valor a validationResult.
     *
     * @param validationResult El valor a asignar a validationResult.
     */
    public void setValidationResult(Map<String, String> validationResult) {
    
    	this.validationResult = validationResult;
    }

	
    /**
     * Asigna un nuevo valor a generalErrors.
     *
     * @param generalErrors El valor a asignar a generalErrors.
     */
    public void setGeneralErrors(List<String> generalErrors) {
    
    	this.generalErrors = generalErrors;
    }
	
    /**
     * Agrega un String a la lista de generalErrors.
     *
     * @param generalErrorString El valor a agregar a generalErrors.
     */
    public void addGeneralError(String generalErrorString) {
    	this.generalErrors.add(generalErrorString);
    }
	
    
    /**
	 * Metodo util para convertir los errores de validacion en request de spring mvc a un mapa para usar en el jsp, 
	 * sin que sea intrusivo y asi no tener que usar los tags de spring. Si el controlador
	 * no llama a este metodo, se puede acceder a los errores en request de la forma tradicional de springMVC.
	 * Este metodo es igual a con
	 * la diferencia de que devuelve un mapa cuyo key es el nombre del campo a validar y cuyo valor
	 * es el mensaje de validacion totalmente resuelto como un String.
	 * 
	 * @see BaseValidatingController#getErrorMap(BindingResult)
	 * @param bindingResult
	 * @return
	 */
	public static  Map<String, String> getFieldErrorsStringMap(BindingResult bindingResult, MessageSource resource) {
		Map<String, String> errorMessages = new HashMap<String, String>();
		List<FieldError> fieldErrors = bindingResult.getFieldErrors();
		for (FieldError fieldError : fieldErrors) {
			
			String key = fieldError.getField();
			try {
				String value = resource.getMessage(fieldError.getCodes()[0], null, 
						fieldError.getDefaultMessage(), Locale.getDefault()); 
				
				System.out.println(fieldError.getCodes()[0]);
				errorMessages.put(key, value);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		return errorMessages;
	}

	/**
	 * Getter del atributo containErrors.
	 *
	 * @return el atributo containErrors.
	 */
	public Boolean getContainErrors() {
		return containErrors;
	}

	/**
	 * Setter del atributo containErrors.
	 *
	 * @param containErrors el atributo containErrors a establecer.
	 */
	public void setContainErrors(Boolean containErrors) {
		this.containErrors = containErrors;
	}

	/**
	 * Getter del atributo headMessagePopup.
	 *
	 * @return el atributo headMessagePopup.
	 */
	public String getHeadMessagePopup() {
		return headMessagePopup;
	}

	/**
	 * Setter del atributo headMessagePopup.
	 *
	 * @param headMessagePopup el atributo headMessagePopup a establecer.
	 */
	public void setHeadMessagePopup(String headMessagePopup) {
		this.headMessagePopup = headMessagePopup;
	}
	
	
	
	
}
