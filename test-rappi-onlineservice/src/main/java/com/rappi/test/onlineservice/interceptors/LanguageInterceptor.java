package com.rappi.test.onlineservice.interceptors;

import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 * 
 * Clase encargada de Interceptar las peticiones y colocar la ubicacion adecuada.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 */
@Component
public class LanguageInterceptor extends LocaleChangeInterceptor {
	
	/** Los mensajes. **/
	@Autowired
	private MessageSource messages;

	/*
	 * (non-Javadoc)
	 * @see org.springframework.web.servlet.i18n.LocaleChangeInterceptor#preHandle
	 * (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ServletException {
		boolean b = super.preHandle(request, response, handler);
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		Locale.setDefault(localeResolver.resolveLocale(request));
		return b;
	}

}
