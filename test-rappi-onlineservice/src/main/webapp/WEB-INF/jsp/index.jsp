<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1" />
    
<spring:url value="/resources/css/bootstrap.min.css" var="bootstrapMin" />

<spring:url value="/resources/css/jquery-ui.min.css" var="jqueryUiCss" />
<spring:url value="/resources/js/bootstrap.min.js" var="bootstrapJSMin" />
<spring:url value="/resources/js/jquery.min.js" var="jqueryMin" />
<spring:url value="/resources/js/jquery-ui.min.js" var="jqueryUiMin" />

<spring:url value="/resources/js/validator.min.js" var="validatorMinJS" />

<spring:url value="/resources/js/rappi.js" var="rappiJS" />

<link rel="stylesheet" href="${bootstrapMin}">


<script src="${jqueryMin}"></script>
<script src="${bootstrapJSMin}"></script>
<script src="${jqueryUiMin}"></script>
<script src="${rappiJS}"></script>
</head>
<body>
<form id = "formSummationCube">
<div class="form-group">
  <label for="input">Datos de entrada:</label>
  <textarea class="form-control" rows="5" id="input" name="input"></textarea>
  <button type="button" class="btn btn-default" id="btnAceptar">Enviar</button>
</div> 
<div class="form-group">  
  <label for="result">Resultado</label>
  <textarea class="form-control" rows="5" id="result" name="result"></textarea>
</div>
</form>

</body>
</html>