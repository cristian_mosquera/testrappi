function createErrorMessages(parent, messages) {
	createMessages(parent, messages, 'danger', 'warning-sign') 
}

function createSuccessMessages(parent, messages) {
	createMessages(parent, messages, 'success', 'warning-sign') 
}

function createMessages(parent, messages, cssMessage, cssIcon) {
	$(parent).empty();
    var alertDiv = $("<div class='alert alert-dismissable alert-" + cssMessage + "'>"
            + "<button type='button' class='close' data-dismiss='alert'>×</button></div>");
    $.each(messages, function(i, value) {
        var span = "<div><i class='icon-" + cssIcon + "'></i> " + value + "<div>";
        alertDiv.append(span);
    });
    $(parent).append(alertDiv);
}


function createMessages(parent, messages) {
	$(parent).empty();
    var alertDiv = "";
    $.each(messages, function(i, value) {
    	alertDiv += "<div>" + value + "</div>";
    });
    
    
    $(parent).append(alertDiv);
}

function createGeneralMessages(parent, messages) {
	$(parent).empty();
    var alertDiv = "";
    $.each(messages, function(i, value) {
    	alertDiv +=  value  ;
    });
    
    
    $(parent).append(alertDiv);
}

/**
 * Agrega un error a un contenedor de errores, si es el primero, formatea el
 * contenedor
 * 
 * @param parent
 *            El contenedor de los errores
 * @param msg
 *            El mensaje de error que se agregará
 */
function appendErrorMessage(parent, value) {
    if (parent.find(".alert").length == 0) {
        var alertDiv = $("<div class='alert alert-dismissable alert-danger'>"
                + "<button type='button' class='close' data-dismiss='alert'>×</button></div>");
        $(parent).append(alertDiv);
    }
    var span = "<div><i class='icon-warning-sign'></i> " + value + "<div>";
    parent.find(".alert").append(span);
}



$(function() {
	$("#btnAceptar").click(function(){
		calculateSummationCube();
	})
});
function calculateSummationCube() {
	var formData = $('#formSummationCube').serializeArray();									
	$.ajax({
		url : "/rappi/calculateSummation",
		data : formData,
		type : 'POST',
		async: false,
		dataType : "json",
		complete : function(data) {
			parseResults(data.responseJSON);
		}
	});
}

function parseResults(searchResults) {
	$("#result").html('')
	if (eval(searchResults.containErrors)) {
		if (searchResults.generalErrors.length > 0) {
			createGeneralMessages($("#result"),
				searchResults.generalErrors);
		return false
		}
		if (!$.isEmptyObject(searchResults.validationResult)) {
			$.each(searchResults.validationResult, function(i, error) {
				createMessages($("#result"),
					searchResults.validationResult);
				
			});
		return false;
		}

	} 
	else {
		$('#result').html(searchResults.content)
	}

}
