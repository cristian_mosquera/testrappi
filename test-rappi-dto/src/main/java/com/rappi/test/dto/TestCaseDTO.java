/**
 * 
 */
package com.rappi.test.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Clase encargada de representar los datos del calculo del cubo.
 * 
 * @author <a href = "mailto:mosquerapuello@gmail.com">Cristian José Mosquera Puello</a>
 *
 */
public class TestCaseDTO implements Serializable {

	/** Uuid Serialiable. **/
	private static final long serialVersionUID = 808178230045319597L;
	
	/** El valor de la dimencion para la matriz. **/
	private int n;
	/** El numero de operaciones. **/
    private int m;
    
	/** Los comandos a ejecutar. **/
    private List<String> commands;

	/**
	 * Getter del atributo n.
	 *
	 * @return el atributo n.
	 */
	public int getN() {
		return n;
	}

	/**
	 * Setter del atributo n.
	 *
	 * @param n el atributo n a establecer.
	 */
	public void setN(int n) {
		this.n = n;
	}

	/**
	 * Getter del atributo m.
	 *
	 * @return el atributo m.
	 */
	public int getM() {
		return m;
	}

	/**
	 * Setter del atributo m.
	 *
	 * @param m el atributo m a establecer.
	 */
	public void setM(int m) {
		this.m = m;
	}

	/**
	 * Getter del atributo commands.
	 *
	 * @return el atributo commands.
	 */
	public List<String> getCommands() {
		return commands;
	}

	/**
	 * Setter del atributo commands.
	 *
	 * @param commands el atributo commands a establecer.
	 */
	public void setCommands(List<String> commands) {
		this.commands = commands;
	}
    
    


}
