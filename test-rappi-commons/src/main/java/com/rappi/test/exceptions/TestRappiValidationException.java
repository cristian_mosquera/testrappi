/**
 * 
 */
package com.rappi.test.exceptions;

import java.io.Serializable;

import com.rappi.test.commons.ConstantsMessages;

/**
 * Clase encargada de definir la excepcion en caso de validacion de logica de negocio
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 */
public class TestRappiValidationException extends RuntimeException {

	/** UUid serializable. **/
	private static final long serialVersionUID = 93929097779645085L;
	
	/** Almacena el codigo del error. **/
	private Integer errorCode;
	
	/** Objeto que hace referencia el error**/
	private Serializable referenceObject;
	
	/** Parametros del mensaje **/
	private String [] parameters;

	/**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public TestRappiValidationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public TestRappiValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public TestRappiValidationException(String message) {
        super(message);
    }
    
    /**
     * Constructor.
     *
     * @param message error message.
     */
    public TestRappiValidationException(Integer errorCode) {
        super(ConstantsMessages.MESSAGES.get(errorCode));
        this.errorCode = errorCode;
    }
    
    
	/**
	 * Constructor.
	 * 
	 * @param parameters
	 *            los parametros dinamicos del mensaje.
	 * @param errorCode
	 *            Codigo del error.
	 */
    public TestRappiValidationException(Integer errorCode, String parameters[]) {
    	this(errorCode);
    	this.parameters = parameters;
    }
    
    /**
     * Constructor.
     *
     * @param message error message.
     */
    public TestRappiValidationException(Integer errorCode, Serializable referenceObject) {
        super(ConstantsMessages.MESSAGES.get(errorCode));
        this.errorCode = errorCode;
        this.referenceObject = referenceObject;
    }

	/**
	 * Getter del atributo errorCode.
	 *
	 * @return el atributo errorCode.
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * Getter del atributo referenceObject.
	 *
	 * @return el atributo referenceObject.
	 */
	public Serializable getRefrenceObject() {
		return referenceObject;
	}

	/**
	 * Getter del atributo parameters.
	 *
	 * @return el atributo parameters.
	 */
	public String[] getParameters() {
		return parameters;
	}
    
	
	
    
}
