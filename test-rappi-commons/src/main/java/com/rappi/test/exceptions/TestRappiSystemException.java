/**
 * 
 */
package com.rappi.test.exceptions;

import com.rappi.test.commons.ConstantsMessages;

/**
 * Clase encargada de definir la excepcion en caso de errores del sistema.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 */
public class TestRappiSystemException extends Exception {
	
	 /**. UUid serializable**/
	private static final long serialVersionUID = 7319709859164174292L;
	
	/** Mensaje en texto claro. **/
	private String plainText = ConstantsMessages.MESSAGES.
					get(ConstantsMessages.FAILED_CODE_MESSAGE_UN_EXPECTED_ERROR);

	/**
     * Constructor.
     *
     * @param cause cause the error.
     */
    public TestRappiSystemException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     * @param cause cause the error.
     */
    public TestRappiSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message error message.
     */
    public TestRappiSystemException(String message) {
        super(message);
    }

	/**
	 * Getter del atributo plantTeext.
	 *
	 * @return el atributo plantTeext.
	 */
	public String getPlainText() {
		return plainText;
	}
    
    
	
}
