/**
 * 
 */
package com.rappi.test.commons;

import java.util.HashMap;

/**
 * Clase encargada de Definir los mensajes de error.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 */
public class ConstantsMessages {
	
	
	
	public static final HashMap<Integer, String> 
			MESSAGES = new HashMap<Integer, String>();
	
	/* Codigos de mensajes */


	public static final Integer FAILED_CODE_ERROR_EXCEDED_MAX_TEST_CASE = 1;

	public static final Integer FAILED_CODE_ERROR_EXCEDED_MIN_TEST_CASE = 2;

	public static final Integer FAILED_CODE_ERROR_INVALID_RANGE = 3;

	public static final Integer FAILED_CODE_ERROR_EXCEDED_MAX_OPERATIONS = 4;

	public static final Integer FAILED_CODE_ERROR_EXCEDED_MIN_OPERATIONS = 5;

	public static final Integer FAILED_CODE_ERROR_EXCEDED_MAX_DIMENTION = 6;

	public static final Integer FAILED_CODE_ERROR_EXCEDED_MIN_DIMENTION = 7;
	
	public static final Integer FAILED_CODE_MESSAGE_UN_EXPECTED_ERROR = 8;
	
	
	/* Valor de mensajes */
	
	public static final String FAILED_ERROR_EXCEDED_MAX_TEST_CASE = 
									"failed.error.exceded.max.test.case";

	public static final String FAILED_ERROR_EXCEDED_MIN_TEST_CASE = 
										"failed.error.exceded.min.test.case";

	public static final String FAILED_ERROR_INVALID_RANGE = 
										"failed.error.invalid.range";

	public static final String FAILED_ERROR_EXCEDED_MAX_OPERATIONS = 
									"failed.error.exceded.max.operations";

	public static final String FAILED_ERROR_EXCEDED_MIN_OPERATIONS = 
							"failed.error.exceded.min.operations";

	public static final String FAILED_ERROR_EXCEDED_MAX_DIMENTION = 
									"failed.error.exceded.max.dimention";

	public static final String FAILED_ERROR_EXCEDED_MIN_DIMENTION = 
									"failed.error.exceded.min.dimention";
	
	public static final String FAILED_MESSAGE_UN_EXPECTED_ERROR = 
						"failed.code.message.un.expected.error";
	
	
	
	static {
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MAX_TEST_CASE, 
								FAILED_ERROR_EXCEDED_MAX_TEST_CASE);
		
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MIN_TEST_CASE, 
				FAILED_ERROR_EXCEDED_MIN_TEST_CASE);
		
		MESSAGES.put(FAILED_CODE_ERROR_INVALID_RANGE, 
								FAILED_ERROR_INVALID_RANGE);
		
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MAX_OPERATIONS, 
				FAILED_ERROR_EXCEDED_MAX_OPERATIONS);
		
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MIN_OPERATIONS, 
				FAILED_ERROR_EXCEDED_MIN_OPERATIONS);
		
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MAX_DIMENTION, 
				FAILED_ERROR_EXCEDED_MAX_DIMENTION);
		
		MESSAGES.put(FAILED_CODE_ERROR_EXCEDED_MIN_DIMENTION, 
				FAILED_ERROR_EXCEDED_MIN_DIMENTION);
		
		MESSAGES.put(FAILED_CODE_MESSAGE_UN_EXPECTED_ERROR, 
				FAILED_MESSAGE_UN_EXPECTED_ERROR);
				
		
	}
		
		
	

}
