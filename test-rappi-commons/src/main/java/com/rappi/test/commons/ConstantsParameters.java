/**
 * 
 */
package com.rappi.test.commons;

/**
 * Clase encargada de Definir las constantes de keys de parametros.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jose
 *         Mosquera Puello</a>
 *
 */
public interface ConstantsParameters {

	String UPDATE_OPERATION = "UPDATE ";

	String QUERY_OPERATION = "QUERY ";

	int MAX_TEST_CASES = 50;

	int MIN_TEST_CASES = 1;

	int MAX_OPERATIONS = 1000;
	
	int MIN_OPERATIONS = 1;

	int MAX_DIMENTION = 100;
	
	int MIN_DIMENTION = 1;
	
}
