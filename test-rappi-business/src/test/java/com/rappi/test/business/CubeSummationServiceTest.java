/**
 * 
 */
package com.rappi.test.business;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.rappi.test.business.api.CubeSummationService;
import com.rappi.test.dto.TestCaseDTO;
import com.rappi.test.exceptions.TestRappiValidationException;



/**
 * Clase encargada de realizar los casos de prueba de la sumarizacion de cubo.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian José Mosquera Puello</a>
 *
 */
@Test
public class CubeSummationServiceTest  extends AbstractTest{
	
	/** El servicio del calculo de cubo. **/
	@Autowired
	public CubeSummationService cubeSummationService;
	
	
	
	/***
	 * 
	 * Método responsable de probar la funcionalidad getTestCases.
	 *
	 * @throws Exception
	 *             en caso de cuqlquier error.
	 */
	@Test
	public void testGetTestCases() throws Exception {
		   String input = "2\r\n"
	        		+"4 5\r\n"
	        		+"UPDATE 2 2 2 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"UPDATE 1 1 1 23\r\n"
	        		+"QUERY 2 2 2 4 4 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"2 4\r\n"
	        		+"UPDATE 2 2 2 1\r\n"
	        		+"QUERY 1 1 1 1 1 1\r\n"
	        		+"QUERY 1 1 1 2 2 2\r\n"
	        		+"QUERY 2 2 2 2 2 2";
		List<TestCaseDTO> testCases = cubeSummationService.getTestCases(input);
		Assert.assertFalse(testCases.isEmpty());
		Assert.assertEquals(testCases.size(), 2);
		Iterator<TestCaseDTO> iterator = testCases.iterator();
		TestCaseDTO testCaseDTO1 = iterator.next();
		Assert.assertEquals(testCaseDTO1.getN(), 4);
		Assert.assertEquals(testCaseDTO1.getM(), 5);
		Assert.assertTrue(testCaseDTO1.getCommands().
									contains("UPDATE 2 2 2 4"));
		Assert.assertFalse(testCaseDTO1.getCommands().
				contains("UPDATE 2 2 2 1"));
		
		TestCaseDTO testCaseDTO2 = iterator.next();
		Assert.assertEquals(testCaseDTO2.getN(), 2);
		Assert.assertEquals(testCaseDTO2.getM(), 4);
		Assert.assertTrue(testCaseDTO2.getCommands().
									contains("UPDATE 2 2 2 1"));
		Assert.assertFalse(testCaseDTO2.getCommands().
				contains("UPDATE 2 2 2 4"));
		
		
	}
	
	/**
	 * 
	 * Método responsable de realizar la prueba de valdiacion maximo numero de
	 * testcases.
	 *
	 * @throws Exception en caso de cualquier error.
	 */
	@Test(expectedExceptions = TestRappiValidationException.class)
	public void testGetTestCasesValidation() throws Exception {
		   String input = "100\r\n"
	        		+"4 5\r\n"
	        		+"UPDATE 2 2 2 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"UPDATE 1 1 1 23\r\n"
	        		+"QUERY 2 2 2 4 4 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"2 4\r\n"
	        		+"UPDATE 2 2 2 1\r\n"
	        		+"QUERY 1 1 1 1 1 1\r\n"
	        		+"QUERY 1 1 1 2 2 2\r\n"
	        		+"QUERY 2 2 2 2 2 2";
		cubeSummationService.getTestCases(input);
	}
	
	/**
	 * 
	 * Método responsable de realizar la prueba de valdiacion minimo numero de
	 * testcases.
	 *
	 * @throws Exception en caso de cualquier error.
	 */
	@Test(expectedExceptions = TestRappiValidationException.class)
	public void testGetTestCasesValidationMin() throws Exception {
		   String input = "0\r\n"
	        		+"4 5\r\n"
	        		+"UPDATE 2 2 2 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"UPDATE 1 1 1 23\r\n"
	        		+"QUERY 2 2 2 4 4 4\r\n"
	        		+"QUERY 1 1 1 3 3 3\r\n"
	        		+"2 4\r\n"
	        		+"UPDATE 2 2 2 1\r\n"
	        		+"QUERY 1 1 1 1 1 1\r\n"
	        		+"QUERY 1 1 1 2 2 2\r\n"
	        		+"QUERY 2 2 2 2 2 2";
		cubeSummationService.getTestCases(input);
	}
	
	
	
	/***
	 * 
	 * Método responsable de probar la funcionalidad getTestCase.
	 *
	 * @throws Exception
	 *             en caso de cuqlquier error.
	 */
	@Test
	public void testGetTestCase() throws Exception {
		
		String input = "2\r\n"
        		+"4 5\r\n"
        		+"UPDATE 2 2 2 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"UPDATE 1 1 1 23\r\n"
        		+"QUERY 2 2 2 4 4 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"2 4\r\n"
        		+"UPDATE 2 2 2 1\r\n"
        		+"QUERY 1 1 1 1 1 1\r\n"
        		+"QUERY 1 1 1 2 2 2\r\n"
        		+"QUERY 2 2 2 2 2 2";
		TestCaseDTO testCaseDTO1 = cubeSummationService.getTestCase(0, input);
		Assert.assertNotNull(testCaseDTO1);
		Assert.assertEquals(testCaseDTO1.getN(), 4);
		Assert.assertEquals(testCaseDTO1.getM(), 5);
		Assert.assertTrue(testCaseDTO1.getCommands().
									contains("UPDATE 2 2 2 4"));
		Assert.assertFalse(testCaseDTO1.getCommands().
				contains("UPDATE 2 2 2 1"));
		TestCaseDTO testCaseDTO2 = cubeSummationService.getTestCase(1, input);
		Assert.assertNotNull(testCaseDTO2);
		Assert.assertEquals(testCaseDTO2.getN(), 2);
		Assert.assertEquals(testCaseDTO2.getM(), 4);
		Assert.assertTrue(testCaseDTO2.getCommands().
									contains("UPDATE 2 2 2 1"));
		Assert.assertFalse(testCaseDTO2.getCommands().
				contains("UPDATE 2 2 2 4"));
	}
	
	
	/***
	 * 
	 * Método responsable de probar la funcionalidad executeCommand.
	 *
	 * @throws Exception
	 *             en caso de cuqlquier error.
	 */
	@Test
	public void testExecuteCommand() throws Exception {
		
		String input = "2\r\n"
        		+"4 5\r\n"
        		+"UPDATE 2 2 2 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"UPDATE 1 1 1 23\r\n"
        		+"QUERY 2 2 2 4 4 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"2 4\r\n"
        		+"UPDATE 2 2 2 1\r\n"
        		+"QUERY 1 1 1 1 1 1\r\n"
        		+"QUERY 1 1 1 2 2 2\r\n"
        		+"QUERY 2 2 2 2 2 2";
		TestCaseDTO testCaseDTO1 = cubeSummationService.getTestCase(0, input);
		Assert.assertNotNull(testCaseDTO1);
		Assert.assertEquals(testCaseDTO1.getN(), 4);
		Assert.assertEquals(testCaseDTO1.getM(), 5);
		Assert.assertTrue(testCaseDTO1.getCommands().
									contains("UPDATE 2 2 2 4"));
		Assert.assertFalse(testCaseDTO1.getCommands().
				contains("UPDATE 2 2 2 1"));
		TestCaseDTO testCaseDTO2 = cubeSummationService.getTestCase(1, input);
		Assert.assertNotNull(testCaseDTO2);
		Assert.assertEquals(testCaseDTO2.getN(), 2);
		Assert.assertEquals(testCaseDTO2.getM(), 4);
		Assert.assertTrue(testCaseDTO2.getCommands().
									contains("UPDATE 2 2 2 1"));
		Assert.assertFalse(testCaseDTO2.getCommands().
				contains("UPDATE 2 2 2 4"));
		String expectedResult1 = "4427";
		
		String expectedResult2 =  "011";
		String concatResult = "";
		CubeSummationServiceImpl.matrix3d = 
				new String[testCaseDTO1.getN()][testCaseDTO1.getN()]
														[testCaseDTO1.getN()];
		for (String command : testCaseDTO1.getCommands()) {
			String result = cubeSummationService.
					executeCommand(command, testCaseDTO1.getN());
			if (result != null ) {
				concatResult += result ;	
			}
		}
		Assert.assertEquals(concatResult, expectedResult1);
		concatResult = "";
		
		CubeSummationServiceImpl.matrix3d = 
				new String[testCaseDTO2.getN()][testCaseDTO2.getN()]
														[testCaseDTO2.getN()];
		for (String command : testCaseDTO2.getCommands()) {
			String result = cubeSummationService.
				executeCommand(command, testCaseDTO2.getN());
			if (result != null ) {	
				concatResult += result ;	
			}
			 
		}
		
		Assert.assertEquals(concatResult, expectedResult2);
		
		
	}
	
	/***
	 * 
	 * Método responsable de probar la funcionalidad executeCommand.
	 *
	 * @throws Exception
	 *             en caso de cuqlquier error.
	 */
	@Test
	public void testCalculateSummation() throws Exception {
		String input = "2\r\n"
        		+"4 5\r\n"
        		+"UPDATE 2 2 2 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"UPDATE 1 1 1 23\r\n"
        		+"QUERY 2 2 2 4 4 4\r\n"
        		+"QUERY 1 1 1 3 3 3\r\n"
        		+"2 4\r\n"
        		+"UPDATE 2 2 2 1\r\n"
        		+"QUERY 1 1 1 1 1 1\r\n"
        		+"QUERY 1 1 1 2 2 2\r\n"
        		+"QUERY 2 2 2 2 2 2";
		
		String response = cubeSummationService.calculateSummation(input);
		
		String expectedResponse = "4\r\n4\r\n27\r\n0\r\n1\r\n1";
		
		Assert.assertEquals(response, expectedResponse);
	}
}


