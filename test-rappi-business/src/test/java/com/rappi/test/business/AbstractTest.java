	/**
 * 
 */
package com.rappi.test.business;


import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;


/**
 * 
 * Clase encargada de definir una api commun para el manejo de pruebas unitarias.
 * 
 * @author <a href = "mailto:cristian.mosquera@vasslatam.com">Cristian Jos� Mosquera Puello</a>
 *
 */
@ContextConfiguration(locations={"classpath:applicationContext-test.xml"})
public abstract class AbstractTest extends AbstractTestNGSpringContextTests {
	
	
	
}
