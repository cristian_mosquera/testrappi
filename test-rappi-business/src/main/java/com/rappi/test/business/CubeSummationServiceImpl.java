/**
 * 
 */
package com.rappi.test.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.rappi.test.business.api.CubeSummationService;
import com.rappi.test.commons.ConstantsMessages;
import com.rappi.test.commons.ConstantsParameters;
import com.rappi.test.dto.TestCaseDTO;
import com.rappi.test.exceptions.TestRappiSystemException;
import com.rappi.test.exceptions.TestRappiValidationException;

/**
 * Clase encargada de implementar el contrato para las operaciones de sumarizacion
 * del cubo.
 * 
 * @author <a href = "mailto:mosquerapuello@gmail.com">Cristian José Mosquera Puello</a>
 *
 */
@Service
public class CubeSummationServiceImpl implements CubeSummationService {
	
	/** La matriz 3d generada. **/
	public static String matrix3d [][][];
	
	/** El log de la clase. **/
	private static final Logger LOGGER = 
			LoggerFactory.getLogger(CubeSummationServiceImpl.class);
	
	/* (non-Javadoc)
	 * @see com.rappi.test.business.api.CubeSummationService#
	 * getTestCases(java.lang.String)
	 */
	@Override
	public List<TestCaseDTO> getTestCases(String input) 
			throws TestRappiSystemException, TestRappiValidationException {
		try {
			List<TestCaseDTO> testCases = new ArrayList<TestCaseDTO>();
			String lines [] = input.split("\r\n");
	        int countTestCase = Integer.
	        			parseInt(String.valueOf(lines[0]));
	        if (countTestCase > ConstantsParameters.MAX_TEST_CASES) {
	        	throw new TestRappiValidationException
	        	(ConstantsMessages.FAILED_CODE_ERROR_EXCEDED_MAX_TEST_CASE);
	        }
	        else if (countTestCase < ConstantsParameters.MIN_TEST_CASES) {
	        	throw new TestRappiValidationException
	        		(ConstantsMessages.FAILED_CODE_ERROR_EXCEDED_MIN_TEST_CASE);
	        }
	        
	        for (int i = 0; i < countTestCase; i++) {
	            testCases.add(getTestCase(i, input)); 
	        }
	        return testCases;
		}
		catch (TestRappiValidationException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo getTestCases con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw e;
		}
		
		catch (TestRappiSystemException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo getTestCases con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw e;
		}
	
		catch (Exception e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo getTestCases con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw new TestRappiSystemException(message, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.rappi.test.business.api.CubeSummationService#
	 * getTestCase(int, java.lang.String)
	 */
	@Override
	public TestCaseDTO getTestCase(int testCaseNumber, String input)
			throws TestRappiSystemException, TestRappiValidationException {
		try {
			TestCaseDTO tesCaseDTO = new TestCaseDTO();
		       
	        
	        String lines[] = input.split("\r\n");
	        int index = 0;
	        boolean found = Boolean.FALSE;
	        List<String> commands = new ArrayList<String>();
	        
	        for (int i = 0; i < lines.length; i++) {
	        	String line = lines[i];
	        	if (line.length() == 0 || i ==0) {
	        		continue;
	        	}
	            char lineFirstChar = line.charAt(0);
	            if (Character.isDigit(lineFirstChar) ) {
	            	if (index == testCaseNumber && !found) {
	                  String parameters [] = line.split(" ");
	                  tesCaseDTO.setN(Integer.valueOf(parameters[0]));
	                  tesCaseDTO.setM(Integer.valueOf(parameters[1]));
	                  found = Boolean.TRUE;
	                  continue;
	            	}
	            	
	            	if (found) {
	            		break;
	            	}
	            	index ++;
	            }
	            
	            else if (found) {
	                commands.add(line);
	            }
	            
	        }    
	        tesCaseDTO.setCommands(commands);
	        return tesCaseDTO;
		}
		catch (TestRappiValidationException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo getTestCase con los parametros: input=" 
					+ input + ", testCaseNumber=" + testCaseNumber;
			LOGGER.error(message, e);
			throw e;
		}
		
	
		catch (Exception e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo getTestCase con los parametros: input=" 
					+ input + ", testCaseNumber=" + testCaseNumber;
			LOGGER.error(message, e);
			throw new TestRappiSystemException(message, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.rappi.test.business.api.CubeSummationService#
	 * executeCommand(java.lang.String)
	 */
	@Override
	public String executeCommand(String command, int n)
			throws TestRappiSystemException, TestRappiValidationException {
		try {
			if (command.contains(ConstantsParameters.UPDATE_OPERATION)) {
	        	executeUpdateCommand(command);
	        }
	        else if (command.contains(ConstantsParameters.QUERY_OPERATION)) {
	        	return executeQueryCommand(command, n);
	        	
	        }
			
			return null;
		}
		catch (TestRappiValidationException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo executeCommand con"
								+ " los parametros: command=" + command;
			LOGGER.error(message, e);
			throw e;
		}
		
	
		catch (Exception e) {
			String message = "ocurrio un error en la aplicacion en el"
										+ " metodo executeCommand con"
									+ " los parametros: command=" + command;
			LOGGER.error(message, e);
			throw new TestRappiSystemException(message, e);
		}

	}

	/**
	 * Método responsable de ejecutar el comando de consulta.
	 *
	 * @param command
	 *            el comando que desea ejecutar.
	 * @param n
	 *            la dimension enviada.
	 * @return el resultado de la consulta.
	 */
	private String executeQueryCommand(String command, int n) {
		command = command.replace(ConstantsParameters.
							QUERY_OPERATION, StringUtils.EMPTY);
		String parameters [] = command.split(" ");
		
		int x1 = Integer.valueOf(parameters[0]) - 1;
		int y1 = Integer.valueOf(parameters[1]) - 1;
		int z1 = Integer.valueOf(parameters[2]) - 1;
		int x2 = Integer.valueOf(parameters[3]) - 1;
		int y2 = Integer.valueOf(parameters[4]) - 1;
		int z2 = Integer.valueOf(parameters[5]) - 1;
		
		if (x1 > x2 || x1 < 0 || x2 > n) {
			throw new TestRappiValidationException
				(ConstantsMessages.FAILED_CODE_ERROR_INVALID_RANGE);
		}
		
		if (y1 > y2 || y1 < 0 || y2 > n) {
			throw new TestRappiValidationException
				(ConstantsMessages.FAILED_CODE_ERROR_INVALID_RANGE);
		}
		
		if (z1 > z2 || z1 < 0 || z2 > n) {
			throw new TestRappiValidationException
				(ConstantsMessages.FAILED_CODE_ERROR_INVALID_RANGE);
		}
		Long sum = 0l;
		for (int i = 0; i< matrix3d.length; i++) {
			for (int j = 0; j< matrix3d[i].length; j++) {
				for (int k = 0; k< matrix3d[i][j].length; k++) {
					if ((i >= x1 && i <= x2) && (j >= y1 && j <= y2) 
											&& (k >= z1 && k <= z2)) {
						if (matrix3d[i][j][k] != null) {
							sum+= Long.parseLong(matrix3d[i][j][k]);
						}	
					}
				}
			}
			
		}
		return sum.toString();
	}

	/**
	 * Método responsable de ejecutar el comando de actualizacion.
	 *
	 * @param command el comando que desea ejecutar.
	 * 
	 */
	private void executeUpdateCommand(String command) {
		command = command.replace(ConstantsParameters.
							UPDATE_OPERATION, StringUtils.EMPTY);
		String parameters [] = command.split(" ");
		int x = Integer.valueOf(parameters[0]) - 1;
		int y = Integer.valueOf(parameters[1]) - 1;
		int z = Integer.valueOf(parameters[2]) - 1;
		
		matrix3d [x][y][z] = parameters[3];
	}

	/* (non-Javadoc)
	 * @see com.rappi.test.business.api.CubeSummationService#
	 * calculateSummation(java.lang.String)
	 */
	@Override
	public String calculateSummation(String input) 
			throws TestRappiSystemException, TestRappiValidationException {
		try {
			String result = StringUtils.EMPTY;
			List<TestCaseDTO> testCases = getTestCases(input);
	        for (TestCaseDTO testCase : testCases) {
	        	if (testCase.getM() > ConstantsParameters.MAX_OPERATIONS) {
		        	throw new TestRappiValidationException
		        	(ConstantsMessages.
		        			FAILED_CODE_ERROR_EXCEDED_MAX_OPERATIONS);
		        }
		        else if (testCase.getM() < ConstantsParameters.MIN_OPERATIONS) {
		        	throw new TestRappiValidationException
		        		(ConstantsMessages.
		        			FAILED_CODE_ERROR_EXCEDED_MIN_OPERATIONS);
		        }
	        	
	        	if (testCase.getN() > ConstantsParameters.MAX_DIMENTION) {
		        	throw new TestRappiValidationException
		        	(ConstantsMessages.
		        			FAILED_CODE_ERROR_EXCEDED_MAX_DIMENTION);
		        }
		        else if (testCase.getN() < ConstantsParameters.MIN_DIMENTION) {
		        	throw new TestRappiValidationException
		        		(ConstantsMessages.
		        			FAILED_CODE_ERROR_EXCEDED_MIN_DIMENTION);
		        }
	        	matrix3d = new String[testCase.getN()]
	        			[testCase.getN()][testCase.getN()];
	        	for (String command: testCase.getCommands()) {
	        		String response = executeCommand(command, testCase.getN());
	        		if (response != null) {
	        			if (StringUtils.EMPTY.equals(result)) {
	        				result += executeCommand(command, testCase.getN());
	        			}
	        			else {
	        				result += "\r\n" + executeCommand
	        							(command, testCase.getN());
	        			}
	        		}
	        		
	        	}	
	        }
	        return result;
		}
		
		catch (TestRappiValidationException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo calculateSummation "
					+ "	con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw e;
		}
		
		catch (TestRappiSystemException e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo calculateSummation "
					+ "	con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw e;
		}
	
		catch (Exception e) {
			String message = "ocurrio un error en la aplicacion en el"
					+ " metodo calculateSummation "
					+ "con los parametros: input=" + input;
			LOGGER.error(message, e);
			throw new TestRappiSystemException(message, e);
		}
		
	}

}
